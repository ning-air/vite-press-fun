export default {
  themeConfig: {
    // 标题配置
    siteTitle: "NING 博客站点",
    logo: "/logo.png",

    // 导航配置
    nav: [
      { text: "Skill points", link: "/guide/", activeMatch: "/guide/" }, //activeMatch: true  自身或子类是否在其中 true高亮
      { text: "vitePress", link: "/vue/", activeMatch: "/vue/" },

      {
        text: "更多",
        items: [
          { text: "Item A", link: "/item-1/" },
          { text: "Item B", link: "/item-2/" },
          { text: "Item C", link: "/item-3/" },
        ],
      },
    ],
    // 导航右侧社交 仅支持以下几种类型
    socialLinks: [
      { icon: "github", link: "https://gitee.com/ning-air/vite-press-dome" },
    ],

    // 侧边栏配置
    sidebar: {
      "/guide/": [
        {
          text: "VUE",
          collapsible: true, //是否带折叠
          collapsed: false,
          items: [
            { text: "about-VUE", link: "/guide/" },
            { text: "VUE-面试高频题", link: "/guide/one" },
            { text: "ES6小技巧", link: "/guide/two" },
          ],
        },
      ],
      "/vue/": [
        {
          text: "Vue",
          collapsible: true, //是否带折叠
          collapsed: false,
          items: [
            { text: "Index", link: "/vue/" },
            { text: "three", link: "/vue/three" },
            { text: "four", link: "/vue/four" },
          ],
        },
      ],
      "/item-1/": [
        {
          text: "Item-1",
          collapsible: true, //是否带折叠
          collapsed: false,
          items: [
            { text: "a", link: "/item-1/a" },
            { text: "b", link: "/item-1/b" },
          ],
        },
      ],

      "/item-2/": [
        {
          text: "Item-2",
          collapsible: true, //是否带折叠
          collapsed: false,
          items: [
            { text: "a", link: "/item-2/a" },
            { text: "b", link: "/item-2/b" },
          ],
        },
      ],

      "/item-3/": [
        {
          text: "Item-3",
          collapsible: true, //是否带折叠
          collapsed: false,
          items: [
            { text: "a", link: "/item-3/a" },
            { text: "b", link: "/item-3/b" },
          ],
        },
      ],
    },
  },
};
