> 开发中高频使用的 ES6 你学废了没

### 1.打乱数组顺序

```javascript
let arr = ["NING", 68, false, true, "88"];
arr = arr.sort(() => 0.5 - Math.random());
console.log(arr);
//[ '88', 68, false, 'NING', true ]
```

### 2.去除数字之外的所有字符

```javascript
const str = "xiaoning 444343 is 123123 so hansome 5455445";
const numbers = str.replace(/\D/g, "");
console.log(numbers);
// 4443431231235455445
```

::: tip 正则表达式
感兴趣正则表达式请参考。**[正则表达式](https://www.w3cschool.cn/zhengzebiaodashi/regexp-syntax.html)**
:::

::: tip 正则表达式
对于正则表达式 (/d+/g,"")：d 表示数字, + 表示一个或多个，就是把连续的多个数字替换为空。
:::

>

### 3.合并多个对象

```javascript

const city = {
    name:"zhuhai"
    population:"9999999"
}
const location = {
    longitude:"113.57",
    latitude:"113.57",
}
const fullCity = {...city, ...location}
console.log(fullCity)

// {
//   name: 'zhuhai',
//   population: '9999999',
//   longitude: '113.57',
//   latitude: '113.57'
// }

```
