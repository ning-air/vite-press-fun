# 什么是 VUE？

> 详细参考官网[官方文档](https://cn.vuejs.org/guide/introduction.html)

- Vue 是一款用于构建用户界面的 JavaScript 框架
- 基于标准 HTML、CSS 和 javaScript 构建，提供声明式、组件化开发模式，高效开发用户界面

# VUE 有哪些特点？

组件化.

轻量级.

虚拟 DOM.

单页面应用.

# 如何理解单页面应用？ 有哪些缺点？

**单页:全称 SPA 单页面应用（SinglePage Web Application）.**

单页应用将所有内容都放在一个页面中，使整个页面丝滑流畅。就用户体验而言，单页导航可以定位锚点，能快速定位到对用的页面部分，并能丝滑上下滚动。单页面应用提供的信息和一些主要内容都通过了筛选和控制，可以简单方便阅读以及浏览。

**缺点：Vue SPA 单页面应用对 SEO 不友好**

::: tip SEO
SEO 是指搜索引擎优化
:::

**为什么说 Vue 单页面应用对 SEO 不友好？**

**1、vue 用 js 来渲染数据对 seo 不友好**

搜索引擎的基础爬虫的原理就是抓取你的 url，然后获取你的 html 源代码并解析。 而你的页面通常用了 vue 等 js 的数据绑定机制来展示页面数据，爬虫获取到的 html 是你的模型页面而不是最终数据的渲染页面，所以说用 js 来渲染数据对 seo 并不友好。

**2、vue 单页页面对 seo 不友好**

seo 本质是一个服务器向另一个服务器发起请求，解析请求内容。但一般来说搜索引擎是不回去执行请求到的 js 的。也就是说，如果一个单页应用，html 在服务器端还没有渲染部分数据数据，在浏览器才渲染出数据，而搜索引擎请求到的 html 是没有渲染数据的。 这样就很不利于内容被搜索引擎搜索到。 所以服务端渲染就是尽量在服务器发送到浏览器前页面上就是有数据的。

**3、vue 数据逻辑操作不合理**

一般的数据逻辑操作是放在后端的。排序这个如果仅仅是几条数据，前后端排序开起来是一样的，如果是有 1000 条数据，前端要排序就要都请求过来。这样显然是不合理的。

**那么如何解决这个 SEO 问题？**

**1.SSR 服务器渲染**

> 服务端渲染, 在服务端 html 页面节点, 已经解析创建完了, 浏览器直接拿到的是解析完成的页面解构关于服务器渲染：Vue 官网介绍 ，对 Vue 版本有要求，对服务器也有一定要求，需要支持 nodejs 环境。优势: 更好的 SEO，由于搜索引擎爬虫抓取工具可以直接查看完全渲染的页面缺点: 服务器 nodejs 环境的要求, 且对原代码的改造成本高! nuxt.js (坑比较多, 做好踩坑的准备

**2.静态化 (博客, 介绍性官网)**

> Nuxt.js 可以进行 generate 静态化打包, 缺点: 动态路由会被忽略。 /users/:id 优势：编译打包时, 就会帮你处理, 纯静态文件，访问速度超快；对比 SSR，不涉及到服务器负载方面问题；静态网页不宜遭到黑客攻击，安全性更高。不足：如果动态路由参数多的话不适用。

**3.预渲染 prerender-spa-plugin (插件)**

> 使用请参考 **[prerender-spa-plugin (插件)](https://dandelioncloud.cn/article/details/1514178278226214913)**

> 如果你只是对少数页面需要做 SEO 处理（例如 / 首页, /about 关于等页面）预渲染是一个非常好的方式, 预渲染会在构建时, 简单的针对特定路由, 生成静态 HTML 文件 (打包时可以帮你解析静态化)优势: 设置预渲染简单, 对代码的改动小缺点: 只适合于做少数页面进行 SEO 的情况, 如果页面几百上千, 就不推荐了 (会打包很慢)

**4.使用 Phantomjs 针对爬虫 做处理**

> Phantomjs 是一个基于 webkit 内核的无头浏览器，没有 UI 界面，就是一个浏览器，其内的点击、翻页等人为相关操作需要程序设计实现。这种解决方案其实是一种旁路机制，原理就是通过 Nginx 配置， 判断访问的来源 UA 是否是爬虫访问，如果是则将搜索引擎的爬虫请求转发到一个 node server，再通过 PhantomJS 来解析完整的 HTML，返回给爬虫
