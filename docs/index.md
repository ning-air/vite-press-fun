---
layout: home

hero:
  name: VitePress-Sky
  text: VitePres体验博客站点
  tagline: Lorem ipsum...
  image:
    src: /cal.png
    alt: VitePress
  actions:
    - theme: brand
      text: Get Started
      link: /guide/index
    - theme: alt
      text: View on GitHub
      link: https://github.com/vuejs/vitepress

features:
  - icon: ⚡️
    title: Vite, The DX that can't be beat
    details: Lorem ipsum...
  - icon: 🖖
    title: Power of Vue meets Markdown
    details: Lorem ipsum...
  - icon: 🛠️
    title: Simple and minimal, always
    details: Lorem ipsum...
---
